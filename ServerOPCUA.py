# -*- coding: utf-8 -*-
"""
Created on Wed Sep  8 08:07:48 2021
@author: guillermosm
"""

import sys
out = sys.stderr
import time
from opcua import Server
sys.stderr = out


#-----------------------------------Variable initialization---------------------------------------------------------------------------------------------------
value_sig1 = 0
value_sig2 = 0
c_value_sig1 = 0
c_value_sig2 = 0
enter = ""
#-----------------------------------Server configuration-------------------------------------------------------------------------------------------------------
print("")
print("-------------------------------------------------------------------------------" )
print("-                            OPC UA Server                                    -" )
print("-------------------------------------------------------------------------------" )
print("")

server=Server()                                                                    #Server definition
url = "opc.tcp://127.0.0.1:49320"
server.set_endpoint(url)
name = "OPCUA"                                                                     #Server Name
addspace = server.register_namespace(name)                                         #Register a new namespace. Nodes should in custom namespace, not 0.
node = server.get_objects_node()                                                   #Get Objects node of the server. Returns a Node object
Param = node.add_object(addspace, "Signals")                                       #Add the object "Parameters" to the node
                                      
#-----------------------------------Readable variables---------------------------------------------------------------------------------------------------------
Point = Param.add_variable(addspace, "Signal1", 0)                                 #1
Segment = Param.add_variable(addspace, "Signal2", 0)                              #2

#-----------------------------------Setting variables as writable----------------------------------------------------------------------------------------------
Point.set_writable()
Segment.set_writable()


#-----------------------------------##################
#-----------------------------------#  MAIN PROGRAM  #
#-----------------------------------##################

if __name__ == "__main__":
#-----------------------------------START the server----------------------------------------------------------------------------------------------------------
        server.start()                                                                    
        print("Server started at {}".format(url))                                          #Print to indicate that server is UP

#-----------------------------------INFINITE LOOP-------------------------------------------------------------------------------------------------------------
        while(True):                                                                   #Continuous loop

            value_sig1 = input("Please, set value for Signal no. 1:\n")
            value_sig2 = input("Please, set value for Signal no. 2:\n")
            Point.set_value(value_sig1)
            Segment.set_value(value_sig2)
            enter = input("Press enter to update the values\n")
            Point.set_value(value_sig1)
            Segment.set_value(value_sig2)
            c_value_sig1 = Point.get_value()
            c_value_sig2 = Segment.get_value()
            print("Current value for Signal no. 1: ", c_value_sig1)
            print("Current value for Signal no. 2: ", c_value_sig2)
            enter = input("Press enter to set new values\n")
            print("")
            time.sleep(1)                                                              #Sleep IN infinite loop
